package com.synergy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackages = {"com.synergy.domain.repository"})
public class SpringLambdaApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(SpringLambdaApplication.class, args);
    }
}
