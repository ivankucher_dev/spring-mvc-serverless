package com.synergy.domain.repository;

import com.synergy.domain.entity.Train;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TrainRepository extends MongoRepository<Train, String> {
    List<Train> findAllByCostBetween(long lowerCost, long upperCost);
}
