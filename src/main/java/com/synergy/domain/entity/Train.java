package com.synergy.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "train")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Train {

    @Id
    private String id;
    @Field("name")
    private String name;
    @Field("cost")
    private long cost;
}
