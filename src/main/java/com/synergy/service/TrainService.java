package com.synergy.service;

import com.synergy.domain.entity.Train;
import com.synergy.domain.repository.TrainRepository;
import com.synergy.rest.dto.TrainDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TrainService {
    private final TrainRepository trainRepository;

    public Train saveTrain(TrainDto trainDto) {
        Train train = new Train();
        train.setCost(trainDto.getCost());
        train.setName(trainDto.getName());

        return trainRepository.save(train);
    }

    public List<Train> getTrainsByCostInRange(long cost, long range) {
        return trainRepository.findAllByCostBetween(cost - range, cost + range);
    }
}
