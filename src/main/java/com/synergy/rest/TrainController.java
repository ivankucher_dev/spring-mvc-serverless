package com.synergy.rest;

import com.synergy.domain.entity.Train;
import com.synergy.rest.dto.ErrorDto;
import com.synergy.rest.dto.TrainDto;
import com.synergy.service.TrainService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.Function;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class TrainController {

    private final TrainService trainService;

    @PostMapping("/saveTrainInfo")
    public Train saveTrainInfo(@RequestBody TrainDto trainDto) {
        return trainService.saveTrain(trainDto);
    }

    @GetMapping("/get-trains-in-cost-range")
    public List<Train> saveTrainInfo(@RequestParam long cost, @RequestParam long range) {
        return trainService.getTrainsByCostInRange(cost, range);
    }
}
