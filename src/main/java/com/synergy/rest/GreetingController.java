package com.synergy.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/greeting")
public class GreetingController {

    @GetMapping("/uuid-hello")
    public ResponseEntity<String> sayHelloWithUUID() {
        return ResponseEntity.ok("hello " + UUID.randomUUID());
    }
}
