package com.synergy.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorDto {
    @JsonProperty("error_message")
    private String errorMessage;
    @JsonProperty("error_details")
    private String errorDetails;
}
